const ham = document.getElementById('ham');
const content = document.getElementById('content');
const datesCollection = document.getElementsByClassName('date');
const dates = Array.from(datesCollection);
const today = new Date();
const dd = String(today.getDate()).padStart(2, '0');
const mm = String(today.getMonth() + 1).padStart(2, '0');
const yyyy = today.getFullYear();

const total = document.getElementById('total');
const adsCollection = document.getElementsByClassName('ad');
const ads = Array.from(adsCollection);

const pagination = document.getElementById('pagination');
const paginationChildren = Array.from(pagination.children);

paginationChildren.forEach(child => {
  child.addEventListener('click', (e) => {
    let current = document.getElementsByClassName('active');
    console.log(current);
    console.log(e.target);
    current[0].removeAttribute('class');
    e.target.className += 'active';
  });
});

total.innerHTML = `Total on page: <b>${ads.length}</b>`;

ham.addEventListener('click', toggleHam);

dates.forEach(date => {
  date.innerHTML = `Posted on ${dd}/${mm}/${yyyy}`;
});

function toggleHam() {
  if (content.style.maxHeight) {
    content.style.maxHeight = null;
  } else {
    content.style.maxHeight = content.scrollHeight + 'px';
  }
  ham.classList.toggle('open');
}
